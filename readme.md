DBM (Database Backup Manager) is a web app designed to manage the backups of multiple databases and configure automated backups process

WIP: this project is in development. In the future more functionalities will be added.

# TODO #
* Delete backup command
* Make backup by database id
* Web interface
* Restore backup (all) command
* Restore table command
* View table data from web